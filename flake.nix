{
  description = "QtQuick home automation example playground using flakes.";

  inputs.nixpkgs.url = "nixpkgs/nixos-20.09";

  outputs = { self, nixpkgs }:
  let
    systems = [ "x86_64-linux" "i686-linux" "x86_64-darwin" "aarch64-linux" ];
    forAllSystems = f: nixpkgs.lib.genAttrs systems (system: f (import nixpkgs {
      inherit system; overlays = [ self.overlay ];
    }));
  in {
    overlay = final: prev: {
      test-ha = final.libsForQt5.callPackage self {};
      libsForQt5 = prev.libsForQt514;
      qt5 = prev.qt514;
    };

    defaultPackage = forAllSystems (pkgs: pkgs.test-ha);

    devShell = forAllSystems (pkgs: import "${self}/shell.nix" {
      inherit pkgs;
      test-ha = pkgs.test-ha;
    });
  };
}
