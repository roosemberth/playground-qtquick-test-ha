{ mkDerivation
, qmake
, qtbase
, qtdeclarative
, qtquickcontrols2
, pkgconfig
}:
mkDerivation {
  pname = "test-ha";
  version = "0.0.1";

  src = ./.;

  buildInputs = [ qtbase qtdeclarative qtquickcontrols2 ];
  nativeBuildInputs = [ qmake pkgconfig ];
  propagatedBuildInputs = [ qtdeclarative ];

  enableParallelBuilding = true;
}
